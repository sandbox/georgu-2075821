<?php
/*
 * @file
 * Administration page callbacks
 */

/*
 * Administration settings form
 */
function flickrimagedisplay_settings_form() {
  $form = array();

  drupal_set_title(t('Flickr Image Display Settings'));

  $form[FlickrImageBlockCount] = array(
    '#type' => 'textfield',
    '#title' => t('Number of blocks'),
    '#description' => t('Fill in the number of blocks that should be generated'),
    '#default_value' => variable_get(FlickrImageBlockCount, 1),
  );
  
  $form['#validate'][] = 'flickrimagedisplay_settings_form_validate';
  $form['#submit'][] = 'flickrimagedisplay_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Administration settings form validator
 */
function flickrimagedisplay_settings_form_validate($form, &$form_state) {
  //module_load_include('module', 'flickrapi');
  //if ($form_state['values'][FlickrImageBlockCount] < 0) {
  //  form_set_error(FlickrImageBlockCount, t('Number of blocks must be greate 0.'));
  //}  
}

/**
 * Custom submit function for the settings form
 */
function flickrimagedisplay_settings_form_submit($form, &$form_state) {
  
}
