<?php 
/*
 * Available vars:
 * - $photos: Array that contains the image and titles with links
 */
?>
<div id='flickrblock'>
  <div id='flickrblock-photos'>
    <?php foreach ($photos as $key => $photo) : ?>
      <div class='flickrblock-wrap'>
        <?php print $photo['image_link']; ?>
        <?php print $photo['title_link'];  ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>